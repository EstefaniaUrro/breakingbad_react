import React, { Component } from 'react';
import { Titulo } from './Titulo';
import Acceder from './Acceder';
import Personajes from './Personajes';



export default class GestorBuscador extends Component {
    constructor() {
        super();
        this.state = { lista: [] }

        Acceder.leerPersonajes()
            .then(respuesta => {
                this.setState({
                    lista: respuesta,
                })
            })
    }
    render() {
        let lista = this.state.lista;
        return (
            <div className="container" >
                <Titulo>BREAKING BAD API</Titulo>
                {
                    lista ?
                        <Personajes datos={lista} />
                        : ""
                }
            </div >
        )
    }
}
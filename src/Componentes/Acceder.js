const PERSONAJES = 'characters';
const API_PELI = `https://breakingbadapi.com/api/`;

export default class Acceder {

    static async leerPersonajes(id = 0) { // 
        let ruta = API_PELI + PERSONAJES;
        if (id !== 0) ruta += "/" + id
        return this.accederApi(ruta);
    }
    static async accederApi(ruta) {
        return await fetch(ruta)
            .then(res => res.json())
            .then(
                results => {
                    console.log(results);
                    return results;
                }
            )
    }
} 
import React, { Component } from 'react';
import Poster from './Poster';

export default class Personajes extends Component {



    render() {

        const lista = this.props.datos;
        return (
            <div>
                <h1 style={{ width: "100%" }}>Se han encontrado {lista && lista.length} personajes</h1>
                <div className="container">
                    {lista.map(personaje => (
                        <div key={personaje.char_id} >
                            <Poster linea={personaje} />
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}
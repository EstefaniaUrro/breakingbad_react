import React, { Component } from 'react';

export default class Poster extends Component {

    render() {

        const linea = this.props.linea
        return (
            <div style={{
                width: "30%",
                height: '425px',
                float: "left",
                marginRight: "20px",
                marginBottom: "20%",
                minWidth: '300px'
            }}>
                <img style={{ width: "100%", height: "400px" }}
                    src={linea.img}
                    alt={linea.name}
                />
                <div >
                    <p className="card-title">{linea.name}</p>
                    <p className="card-text">{linea.birthday}</p>
                    <p className="card-text">{linea.occupation}</p>
                    <p className="card-text">{linea.nickname}</p>
                    <p className="card-text">{linea.status}</p>
                </div>
            </div>

        )
    }
}
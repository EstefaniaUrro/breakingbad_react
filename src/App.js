import React from 'react';

import './App.css';
import GestorBuscador from './Componentes/GestorBuscador';


function App() {

  return (
    <GestorBuscador />
  )
}

export default App;
